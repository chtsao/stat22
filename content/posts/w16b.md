---
title: "Week 16b. One-Sided Hypothesis Tests I"
date: 2022-05-25T11:24:05+08:00
draft: false
tags: [one-sided tests, two-sided tests]
categories: [notes]
---
<img src="https://theglobalcoverage.com/wp-content/uploads/2021/07/SPY-X-FAMILY-Chapter-47-The-Suspense-Intensifies-Release-Date-2048x1297.jpg" style="zoom: 33%;" />

Recall our question in Week 15. [Questions](https://chtsao.gitlab.io/stat22/posts/w15/#questions) : 

> 星七課紅茶容量 700 cc? 

Under "suitable assumption/design of experiment", it is acceptable to formulate the problem as 
a **one-side**d hypothesis testing problem. 

### Formulation
Let $X_1, \cdots, X_n \sim_{iid} N(\mu, \sigma^2)$ with $\mu \in \mathcal{R}$  and  $\sigma^2>0$, both unknown.  

For testing $H_0: \mu \leq \mu_0$ vs. $H_1: \mu > \mu_0$,

a level  $\alpha$ test is to 

* Reject $H_0$ if $\frac{\bar{X}- \mu_0}{S/\sqrt{n}} > t_{n-1, \alpha}$ and claim $H_1$ is supported by data; 

else

* Do not reject $H_0$ if $\frac{\bar{X}- \mu_0}{S/\sqrt{n}} \leq t_{n-1, \alpha}$ and claim $H_1$ is not substantiated. 

Here $t_{n-1, \alpha} $ such that $P(T_{n-1} \leq t_{n-1, \alpha})=1-({\alpha})$ and $T_{n-1} \sim t_{(n-1)} $, a $t$ random variable with $(n-1)$ degrees of freedom; $S^2= \frac{1}{n-1}\sum_{i=1}^n (X_i - \bar{X})^2, S = \sqrt{S^2}$.

##### Quick Exercise (NEW)
Write down the one-sided $\alpha$ level test when Let $X_1, \cdots, X_n \sim_{iid} N(\mu, \sigma^2)$ with $\mu \in \mathcal{R}$  unknown and  $\sigma^2>0$ known. 
You may refer to [Week 15b. Hypothesis Testing II: Z, t and approximately Z](https://chtsao.gitlab.io/stat22/posts/w15b/)
### Example
##### Model 
Assume that the volumes of the black tea from the same batch is normally distributed with mean $\mu$ and variance $\sigma^2$, both unknown. We have

##### Formulation
Let $X_1, \cdots, X_n \sim_{iid} N(\mu, \sigma^2)$ with $\mu \in \mathcal{R}$  and  $\sigma^2>0$, both unknown. Because (*),  the problem is to  test

$H_0: \mu \leq 700$ vs. $H_1: \mu > 700$,

##### Realization/Computation
A random sample of size $16$ is taken and $\bar{x}=704$ (cc), $s^2=64$ and the level of the test $\alpha$ is given as $0.05.$ i.e. $(n, \bar{x}, s^2, \alpha) = (16, 704, 64, 0.05).$

To check whether the volume is greater than $700$ cc, we compute

$\frac{\bar{x}- 700}{s/\sqrt{n}} =\frac{704-700}{8/\sqrt{16}} =2$ and $t_{15, 0.05}=1.753$ by Table B.2. 
##### Conclusion
Thus we reject $H_0: \mu \leq 700$ at level $0.05$ since
$\frac{\bar{x}- 700}{s/\sqrt{n}} =\frac{704-700}{8/\sqrt{16}} =2 > 1.753$ and claim $H_1$ is supported by data. 

In practice, we find the claim of the volume acceptable. 

### Note 
If, however, $\alpha$ is chosen as $0.01$, $t_{15, 0.01} =2.602$. For the same data,  
$\frac{\bar{x}- 700}{s/\sqrt{n}} =\frac{704-700}{8/\sqrt{16}} =2 \leq 2.602$. 
Thus we do not reject $H_0: \mu \leq 700$ at level $0.01$ and claim $H_1$ is not supported by data. 

### Bernoulli Problem: 炭治郎蹺課率 < 30%? 

<img src="http://store.pass345.com/uploads/20201222/7E/7E69C95D6776w1200h628.jpeg" style="zoom:50%;" />

### Quiz 0526: 

1. (*)
2. Given $(n, \bar{x}, s^2, \alpha) = (25, \bar{x}, 16, 0.05),$ what is the smallest integer $\bar{x}$ that we can reject $H_0: \mu < 700$ at level $\alpha=0.05$?
3. 鱗瀧老師替炭治郎設計了一個一萬個小時的魔鬼訓練。透過自主學習方式，炭治郎若好好完成這個訓練，將可成為一位鬼殺隊的優秀武者。以目前進度規劃，如全勤狀況下，炭治郎應該已經完成九成（9000小時）的課程。但幾次觀察，炭治郎都以一些理由，或甚至藉口沒有參加那些訓練。而實際考驗的成效似乎也顯示他沒有好好學習。鱗瀧老師因而懷疑
> 炭治郎蹺課率 $\geq$ 30%

在這樣的理解下，他覺得沒有什麼繼續教導炭治郎的意義了。因此也動了將他逐出門下的想法。知道了這個訊息，彌豆子很緊張，她請你幫忙來設計一個實驗與檢定分析，由抽取部份過去訓練的出席資料來說服鱗瀧老師 炭治郎蹺課率 <30%
請比照之前 紅茶容量 有 700CC的例子，寫出這個問題的 Model, Formulation, Realization/Computations and Conclusion.

<img src="https://image.cache.storm.mg/styles/smg-800x533-fp/s3/media/image/2020/10/26/20201026-055538_U10931_M648654_abb4.jpg?itok=IM1R52va" style="zoom:50%;" />

