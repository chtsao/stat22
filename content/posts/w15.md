---
title: "Week 15a. Hypothesis Testing I: Questions, Concerns and Formulation"
date: 2022-05-17T16:33:29+08:00
draft: false
tags: [hypothesis testing, null hypothesis, alternative hypothesis, significance level]
---
<img src="https://tw.appledaily.com/resizer/SRqwtiYcSwoG7cMHGztOAsnam5s=/759x397/filters:quality(100)/cloudfront-ap-northeast-1.images.arcpublishing.com/appledaily/7JJLX4SWFPGRCKZS4ZIMCI6KTE.jpg" style="zoom:50%;" />

### Course Cloud

因應防疫調整，即日起改以線上方式遠距授課，授課時間=原上課時間。[Course Google Class](https://classroom.google.com/c/NDY4OTI3OTUyMzQ0)

## Hypothesis Testing

### Questions

1. [珍奶的容量 700cc](https://news.ltn.com.tw/news/life/paper/717889) 
2. [魯夫身高](http://smallpopo.com/doc_dFQ4VjNvWEwxWFVPTlo1VmxjNUdHZz09) 175 公分？
3. [某機率商品成功率](https://www.gamersecret.com/2021/12/22/%E5%9B%A0%E6%8F%90%E5%87%BA%E5%BF%A0%E5%AF%A6%E5%8F%8D%E9%A5%8B%E8%A2%AB%E5%91%8A%EF%BC%9F%E9%81%8A%E6%88%B2%E6%A9%98%E5%AD%90%E5%B0%8D%E7%8E%A9%E5%AE%B6%E6%8F%90%E8%A8%B4%E4%BA%8B%E4%BB%B6/) = 30%? 市占率 $>$ 30%? (=10%, $>$10%, [丁特vs橘子](https://forum.gamer.com.tw/C.php?bsn=60076&snA=6643429) )

4. Is the defendant innocent or guilty? ([Juries NEVER Find A Criminal Defendant Innocent](https://medium.com/david-grace-columns-organized-by-topic/juries-never-find-a-criminal-defendant-innocent-d68df70a9685))

### Concerns

在回答上述問題時，應該要有哪些考量？

1. Hypothesis testing: $H_0$ vs. $H_1$ (null hypothesis vs. alternatives)
2. Decision/Test based on the data
3. Guarantee about the test (as a method) and its interpretation

### Formulation
1. 法庭：你是法官
| Truth\Judge  | Innocent  | Guilty  |
|---|---|---|
| Innocent  | $\bigcirc$  | ${\large I}$  |
| Guilty  | ${\large II}$  | $\bigcirc$ |


Type I error = 判有罪｜無罪 ＝（冤）枉；

Type II error = 判無罪｜有罪 ＝ 縱（容）； （ 法官裁判｜真實）

2. 曹操：寧可錯殺一萬，不可錯放萬一
	* 想像自己是曹操，寫出類似上表的 決策圖
	* [政策中的「寧可錯殺，不可錯放」](https://vocus.cc/article/5ebb79a3fd897800014dc0a0) 這篇文章，其實說的就是 $H_0$, $H_1$ 怎麼放的考量

3.  $H_0$ vs. $H_1$ 
| Truth\Decision  | Do not reject $H_0$  | Reject $H_0$  |
|---|---|---|
| $H_0$  | $\bigcirc$  | ${\large I}$  |
| $H_1$ | ${\large II}$  | $\bigcirc$ |

#### On Formulation
* What is formulation, anyway?
   * Google PageRank: [wiki](https://en.wikipedia.org/wiki/PageRank), [Everything You Need to Know about Google PageRank (Why it Still Matters in 2021)](https://www.semrush.com/blog/pagerank/)
   * Machine Learning as/and optimzation: [Optimization and Machine Learning](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwi3o_-v9dPwAhVMwYsBHYz6DaIQFjALegQIExAD&url=https%3A%2F%2Fwww.csie.ntu.edu.tw%2F~cjlin%2Ftalks%2Fitaly_optml.pdf&usg=AOvVaw3ryS7nY4q8LNuJOH4jYbPl)

### Reading

* Textbook: Sec 25.1, 25.3; Sec 26.1
