---
title: "Concluding Recap and Remarks"
date: 2022-06-07T16:15:06+08:00
draft: false
---

![](https://miro.medium.com/max/1220/0*rRCCh8b6BTmZeIjR.jpg)

## Final Exam 
* Exam Problems/Questions will be given on June 14 (二), 1310 in our Google Classroom
* Due date/time will also be announced.

## What have we learned?

* The Question: 

  * Intro to Probability: Languages/models/tools for uncertainty, randomness and risks.

    $$x \leadsto X$$

  * Statistics: Jargons/models/tools for assessing $X$ 

  ​      $x_1, \cdots, x_n \leadsto X$

* Formulation and Theory

  1. $x_1, \cdots, x_n$ (from $X$): Numerical summary and Graphical display (keywords: business analytics, data visualization)
  2. Understand $X$ through $x_1, \cdots, x_n$
     * sampling matters $X_1, \cdots, X_n \sim_{iid} f_\theta$ or $\sim_{iid} X$
     * evaluation and guarantee  
     * Statistical inferences: Point Estimation, Interval Estimation, Hypothesis Testing 

## Road Ahead

* 目前：沙坑練習，沙盤推演 $\Rightarrow$ 真實世界資料分析/建模

  透過資料了解 **過去/現在發生了什麼？**  以及 **未來可能發生什麼**/對應機率/預測保證與信心

* From one to (p+1): $X \leadsto (Y, X_1, \cdots, X_p)$ . 

* Data: $(y_i, x_{i, 1}, \cdots, x_{i, p})_{i=1}^n$ $\sim P_{Y, X}$

* Main Questions: 

  Find (some) feasible $f\ \in  \mathcal{F}$  such that $Y \approx f(X_1, \cdots, X_p)$

  * Focus on Prediction/$Y$: Machine Learning/Deep Learning or aka AI
  * Focus on Relation/$\beta$: say $Y \approx \beta_0 + \beta_1 X_1 + \cdots + \beta_p X_p$. Regression/ANOVA/ANCOVA GLM,GLIM, etc.

* Related Topics/Courses:

  * Regression Analysis, Applied Linear Models, Generalized Linear Models
  * Machine Learning, Applied Multivariate Data Analysis
  * Math Stat I,II; Experimental Design, Time Series Analysis
  * Programming, Coding, R and Python. 

* Domain Knowledge and ... English

![](https://cdn-images-1.medium.com/max/1200/1*2Wym4LA075l3VpPi9XIxRQ.png)

