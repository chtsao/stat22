---
title: "Learning Learning"
date: 2022-03-05T08:11:44+08:00
draft: false
tags: [learn, notes, study]
categories: [remark]
---

<img src="https://i0.wp.com/cdn.business2community.com/wp-content/uploads/2013/11/Screen-shot-2013-11-11-at-11.43.55-AM.png" style="zoom:67%;" />

### 學習學習/專業學習

##  Study less, study smart
* Study Less Study Smart”by Dr. Marty Lobdell: [summary by UAPB](https://www.uapb.edu/sites/www/Uploads/SSC/Study%20Smarter%20Not%20Harder.pdf), [video](https://www.youtube.com/watch?v=IlU-zDU6aQ0)
* [學得更好-更開心-更有效率](https://chtsao.gitlab.io/i2p2019/#%E4%BD%A0%E5%8F%AF%E4%BB%A5%E5%AD%B8%E5%BE%97%E6%9B%B4%E5%A5%BD-%E6%9B%B4%E9%96%8B%E5%BF%83-%E6%9B%B4%E6%9C%89%E6%95%88%E7%8E%87)
* Mike and Matty: Study more effectively.
  * [The REAL Reason Why You Get Bad Grades](https://youtu.be/GJ_o-1bfz-M) (Worst)
  * [Evidence based learning strategies](https://youtu.be/UEJmgaFQUH8) (Best)
  * **Worst** because they are on inputs only 
     * Re-reading
     * Highlighting
     * Summarizing
     * Mnemonics 
  * **Best** because they are difficult $\leadsto$ Brain/Neural Network building
     * Quizzing (Active recall)
     * Spacing (Spaced repetition)
     * Mixing (interleaving/Cross-training)
