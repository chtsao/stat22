---
title: "Week 16c. Testing Normal Mean: Examples"
date: 2022-05-26T19:01:18+08:00
draft: false
tags: [tests, two-sided problem]

---

### Formulation

Let $X_1, \cdots, X_n \sim_{iid} N(\mu, \sigma^2)$ with $\mu \in \mathcal{R}$  and  $\sigma^2>0$, both unknown.  

For testing $H_0: \mu = \mu_0$ vs. $H_1: \mu \neq \mu_0$,

a level  $\alpha$ test is to 

* Reject $H_0$ if $\frac{|\bar{X}- \mu_0|}{S/\sqrt{n}} > t_{n-1, \alpha/2}$ and claim $H_1$ is supported by data; 

else

* Do not reject $H_0$ if $\frac{|\bar{X}- \mu_0|}{S/\sqrt{n}} \leq t_{n-1, \alpha/2}$ and claim $H_1$ is not substantiated. 

Here $t_{n-1, \alpha/2} $ such that $P(T_{n-1} \leq t_{n-1, \alpha/2})=1-({\alpha/2})$ and $T_{n-1} \sim t_{(n-1)} $, a $t$ random variable with $(n-1)$ degrees of freedom; $S^2= \frac{1}{n-1}\sum_{i=1}^n (X_i - \bar{X})^2, S = \sqrt{S^2}$.

### Example: Two-sided problem, unknown variance

##### Model 

Assume that the volumes of the black tea from the same batch is normally distributed with mean $\mu$ and variance $\sigma^2$, both unknown. We have

##### Formulation

Let $X_1, \cdots, X_n \sim_{iid} N(\mu, \sigma^2)$ with $\mu \in \mathcal{R}$  and  $\sigma^2>0$, both unknown. Because (*),  the problem is to  test

$H_0: \mu = 700$ vs. $H_1: \mu \neq 700$,

##### Realization/Computation

A random sample of size $16$ is taken and $\bar{x}=704$ (cc), $s^2=64$ and the level of the test $\alpha$ is given as $0.05.$ i.e. $(n, \bar{x}, s^2, \alpha) = (16, 704, 64, 0.05).$

To check whether the volume is  $700$ cc or not, we compute

$\frac{|\bar{x}- 700|}{s/\sqrt{n}} =\frac{|704-700|}{8/\sqrt{16}} =2$ and $t_{15, 0.05/2}=2.131$ by Table B.2. 

##### Conclusion

Thus we do not reject $H_0: \mu = 700$ at level $0.05$ since
$\frac{|\bar{x}- 700|}{s/\sqrt{n}} =\frac{|704-700|}{8/\sqrt{16}} =2 \leq 2.131$ and claim $H_1$ is not supported by data. 


### Note 

If, however, $\alpha$ is chosen as $0.1$, $t_{15, (0.1)/2} =1.753$. For the same data,  
$\frac{|\bar{x}- 700|}{s/\sqrt{n}} =\frac{|704-700|}{8/\sqrt{16}} =2 > 1.753$. 
Thus we reject $H_0: \mu = 700$ at level $0.1$ and claim $H_1$ is supported by data. 