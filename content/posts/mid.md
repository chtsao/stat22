---
title: "Week 8. 防疫措施/線上授課 Midterm Alert"
date: 2022-04-05T15:36:32+08:00
draft: false
categories: [adm]
---
<img src="https://stanforddaily.com/wp-content/uploads/2017/11/Pink_floyd-768x432.jpg" style="zoom:67%;" />

### 防疫措施/線上授課
* 配合本校[防疫措施](https://rb005.ndhu.edu.tw/p/404-1005-194437-1.php?Lang=zh-tw) 4/6-4/16 採線上上課
* 本課程上課時間不變仍為 Tue 1310-1500; Thr 1610-1700，但以線上方式實施。
* GoogleMeet 連結: https://meet.google.com/mmz-vgzx-hzx

## Midterm 

- 日期: 2021 ~~0412 (二)~~. 因本校防疫措施，暫定延至4/26 （二）.
- 地點： 
- 時間: 1310-1440.
- 範圍：上課及習題內容。建議參考上課筆記與課網大綱。
- 其他：No cheatsheet nor mobiles allowed. Prepare early and Good Luck!
  提早準備，固實會的，加強生疏的，弄懂原來不會的！—-考試不難，會就簡單！*練習題 <[下載>](https://chtsao.gitlab.io/stat21/vmid20.pdf)



