---
title: "Finally, the Final"
date: 2022-06-13T10:52:20+08:00
draft: false
---

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.hfYSJhFRsWEKDR8Lq5zWIgHaEa%26pid%3DApi&f=1)

Take-Home Exam: 0614（二）:1310 Google Classroom 公佈考題。Exam 題目.

* Date
  * **Start**: **0614（二）:1310;** 
  * **Deadline: 0615（三）:1159.** 
* Submit your file to our **Google Classroom Classwork**.
* Format: **One** pdf or html file is preferred. Multiple pics are less welcome.
* **遠距口試**: 為 釐清回答 以及確定 規定 之遵守度，我們會抽樣部份同學遠距口試。請於 **0622 （三）左右**查看學校 gmail 口試通知。

**規定**

1. 可討論，可查課本，可參考筆記，可上網，可問人。(除了課本/筆記外的參考資料/人/網站在可知範圍盡量列出，寫在最後報告/答案中) 
2. 自己寫，自己知道，自己知道自己寫什麼，自己寫什麼自己知道

