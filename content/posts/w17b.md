---
title: "Week 17b. Comparing Two Normal Populations"
date: 2022-05-31T12:13:08+08:00
draft: false
tags: [two-sample design, paried design, normal, two-sample t-test, paired t-test, bernoulli]
categories: [notes]
---

<img src="https://4.bp.blogspot.com/-oGKgDKXXngs/USFbU-Y2j-I/AAAAAAAAA5w/hbGc-M-ik_E/s1600/dual+duel.jpg" style="zoom:67%;" />

## Comparison of two normal means

- Motivating problems:
  - **Two-sample design** Comparing **IQ(A) vs. IQ(B)** where A, B might be two people or two groups, say NDHU vs MEKU. The measures are assumed or considered as normally distributed.
  - **Paired Design** Comparing **score(after) vs score(before)**. The paired scores  may be some performance index, say exam scores or BFP  (body fat  percentage) from the subjects participating some training program, for example (exam(before)$_i$, exam(after)$_i$) is the exam score pair from the $i-$th subject. 
- Formulation
- Designs and Tests: Two-sample design vs. Paired design; Two-sample t-test vs. Paired t-test. 

# Considerations

* Design before data collection/analysis
* Think about how the data can be analyzed, what assumption needed before experiment or data collection.
* Equal sample sizes **do not** justify  paired t-test--- (paired) design does. 
* If possible, practically and ethically, paired-design yields a clearer, simpler  and usually more convincing analysis 

# Formulation

## Paired t-test

Let $(X_i, Y_i), i=1, \ldots, n$ be iid random sample from a paired design where 

* $X_1, \ldots, X_n \sim_{iid} N(\mu_X, \sigma_X^2)$ and 
* $Y_1, \ldots, Y_n \sim_{iid}  N(\mu_Y, \sigma_Y^2)$
* $X$ and $Y$ are usually correlated, no independent assumption needed. 
* Define $D_i = X_i -Y_i$ then $D_1, \ldots, D_n \sim_{iid} N(\mu_D, \sigma_D^2)$


#### One-sided Problem
$H_0: \mu_X \geq \mu_Y$ vs. $H_1: \mu_X < \mu_Y$
i.e.

$H_0: \mu_D \geq 0$ vs. $H_1: \mu_D < 0$

A $\alpha$ level test is then (changing notations as in [Normal with unknown variance](https://chtsao.gitlab.io/stat21/posts/w14b/#normal-with-unknown-variance))
Reject $H_0$ if $\frac{\bar{D}- 0}{S_D/\sqrt{n}} < - t_{n-1, \alpha}$ and claim $H_1$ is supported by data; else

Do not reject $H_0$ if $\frac{\bar{D}- 0}{S_D/\sqrt{n}} \geq - t_{n-1, \alpha}$ and claim $H_1$ is not substantiated. 

Here $t_{n-1, \alpha} $ such that $P(T_{n-1} \leq t_{n-1, \alpha})=1-({\alpha})$ and $T_{n-1} \sim t_{(n-1)} $, a $t$ random variable with $(n-1)$ degrees of freedom; $S_D^2= \frac{1}{n-1}\sum_{i=1}^n (D_i - \bar{D})^2, S_D = \sqrt{S_D^2}$ and $\bar{D} = \frac{1}{n} \sum_{i=1}^n D_i$.

#### Two-sided Problem
$H_0: \mu_X = \mu_Y$ vs. $H_1: \mu_X \neq \mu_Y$
i.e.

$H_0: \mu_D = 0$ vs. $H_1: \mu_D \neq 0$

A $\alpha$ level test is then (changing notations as in [Normal with unknown variance](https://chtsao.gitlab.io/stat21/posts/w14b/#normal-with-unknown-variance))
Reject $H_0$ if $\frac{|\bar{D}- 0|}{S_D/\sqrt{n}} >  t_{n-1, \alpha/2}$ and claim $H_1$ is supported by data; else

Do not reject $H_0$ if $\frac{|\bar{D}- 0|}{S_D/\sqrt{n}} \leq  t_{n-1, \alpha/2}$ and claim $H_1$ is not substantiated. 

Here $t_{n-1, \alpha/2} $ such that $P(T_{n-1} \leq t_{n-1, \alpha/2})=1-({\alpha/2})$ and $T_{n-1} \sim t_{(n-1)} $, a $t$ random variable with $(n-1)$ degrees of freedom; $S_D^2= \frac{1}{n-1}\sum_{i=1}^n (D_i - \bar{D})^2, S_D = \sqrt{S_D^2}$ and $\bar{D} = \frac{1}{n} \sum_{i=1}^n D_i$.


## Two-sample t-test

* $X_1, \ldots, X_m \sim_{iid} N(\mu_X, \sigma_X^2)$ and 
* $Y_1, \ldots, Y_n \sim_{iid}  N(\mu_Y, \sigma_Y^2)$
* $X$ and $Y$ are  independent. 
* The $X$'s and $Y$'s are independently collected from two normal population. Usually $m \neq n$. 

#### One-sided problem
$H_0: \mu_X \geq \mu_Y$ vs. $H_1: \mu_X < \mu_Y$
At level $\alpha$, 
Reject $H_0$ if $\frac{\bar{X}-\bar{Y}}{\sqrt{ \widehat{Var}(\bar{X}-\bar{Y})}}$ is "sufficiently" small
Do not reject $H_0$ if otherwise. 

#### Two-sided problem
$H_0: \mu_X = \mu_Y$ vs. $H_1: \mu_X \neq \mu_Y$
At level $\alpha$, 
Reject $H_0$ if $\frac{|\bar{X}-\bar{Y}|}{\sqrt{ \widehat{Var}(\bar{X}-\bar{Y})}}$ is "sufficiently" large
Do not reject $H_0$ if otherwise. 

