---
title: "About"
date: 2022-02-11T10:31:43+08:00
draft: false
categories: [adm]
---
![Dice](https://www.simplilearn.com/ice9/free_resources_article_thumb/Data-Science-vs.-Big-Data-vs.jpg)

* Curator: Kno Tsao.  Office: SE A411.  Tel: 3520
* Lectures: Tue. 1310-1500, Thr. 1610-1710 @ AE A210
* Google Classroom: [Kno.Stat 2022](https://classroom.google.com/c/NDY4OTI3OTUyMzQ0?cjc=dvau3ee) 
* Office Hours: Mon 14:10-15:00, Thr 15:10-16:00  @ SE A411
* TA Office Hours: 
  * [呂一昕](mailto:610911007@gms.ndhu.edu.tw): Fri 13:00-14:00 @ SE A412
  * [蘇羿豪](mailto:611011102@gms.ndhu.edu.tw): Wed. 15:00-16:00 @SE A408
* Prerequisites: Calculus, Intro to Probability
* Textbook:  Dekking, Kraaikamp, Lopuhaä and Meester (2005). A Modern Introduction to Probability and Statistics: Understanding Why and How. Springer, London. [Legal downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=766570&q=Modern+Introduction+to+Probability+and+STatistics&start=0&view=CONTENT)